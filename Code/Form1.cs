﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV3_LanguageMachine
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MyStaticValues.pathToExport = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\testExport.txt";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LangWorker worker = new LangWorker(textBox1.Text);

            //analysis of entered input
            worker.checkNextDigitTest();

            //Exporting to file on desktop
            worker.exportToTXTFile(MyStaticValues.pathToExport);

            listBox1.Items.Add("line " + MyStaticValues.ExportCounterLangWorker + ": " + textBox1.Text + "\n");
            listBox1.Items.Add("----------------------------------------------------------------------------------");
            List <KeyValuePair<string, string>> syntaxIdentifier = worker.getSyntaxIdentifier();
            for (int i=0; i<syntaxIdentifier.Count; i++) {
                listBox1.Items.Add("(" + "'" + syntaxIdentifier[i].Key + "'" + ", " + syntaxIdentifier[i].Value + ")");
                listBox1.Items.Add("----------------------------------------------------------------------------------");
            }

            Dictionary<string, int> syntaxCounter = worker.getSyntaxCounter();
            foreach (KeyValuePair<string, int> kvp in syntaxCounter) {
                listBox2.Items.Add("Key: " + kvp.Key + "\t Value: " + kvp.Value);
                listBox2.Items.Add("----------------------------------------------------------------------------------");
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
        }

        private void button3_Click(object sender, EventArgs e) {
            MyStaticValues.ExportCounterLangWorker = 0;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(MyStaticValues.pathToExport)) {
                file.Write(String.Empty);
            }
        }
    }
}